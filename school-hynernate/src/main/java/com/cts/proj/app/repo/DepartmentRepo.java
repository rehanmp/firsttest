package com.cts.proj.app.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cts.proj.app.model.Department;

@Repository
public interface DepartmentRepo extends CrudRepository<Department,String>{
	

}
