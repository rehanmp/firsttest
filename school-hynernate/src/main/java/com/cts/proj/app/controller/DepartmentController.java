package com.cts.proj.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cts.proj.app.model.Department;

@Controller
public class DepartmentController {

	@Autowired
	Department service;
	@RequestMapping("/addDepartment")
	@ResponseBody
	public String addDepartment()
	{
		Department dept = new Department();
		dept.setDepartmentID("CSE");
		dept.setDepartmentName("computer science");
		dept.setLocation("Block A");
	
		
		boolean isAdded = service.addDepartment(dept);
		
		return isAdded ? "department added" :"not added" ;
	}	
	
}
