package com.cts.proj.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import com.cts.proj.app.model.Department;
import com.cts.proj.app.repo.DepartmentRepo;

@Service
@ComponentScan("com.cts.proj.app")
public class DepartmentService {
	
	@Autowired
	DepartmentRepo departmentRepo;

	public boolean addDepartment(Department dept)
{
	return departmentRepo.save(dept)!=null;
}
}
